package sebastien.boardgames.models;

import sebastien.boardgames.utils.Database;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author sebastien corsyn AS info 2018-2019
 */
public class Game {
    protected int id;
    protected String title;
    protected double price;
    protected String players;
    protected int instock;
    protected String miniage;

    /**
     * Constructor of a game without attributes
     */ 
    public Game() {
    }
    
    /**
     * Constructor of a game with attributes
     * @param id
     * @param title
     * @param price
     * @param players
     * @param in_stock
     * @param mini_age
     */ 
    public Game(int id, String title, double price, String players, int in_stock, String mini_age) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.players = players;
        this.instock = in_stock;
        this.miniage = mini_age;
    }

    /**
     * Constructor of a game with attributes but not id
     * @param title
     * @param price
     * @param players
     * @param in_stock
     * @param mini_age
     */ 
    public Game(String title, double price, String players, int in_stock, String mini_age) {
        this.title = title;
        this.price = price;
        this.players = players;
        this.instock = in_stock;
        this.miniage = mini_age;
    }

    /**
     * Getter of id
     * @return int id
     */     
    public int getId() {
        return id;
    }

    /**
     * Getter of title
     * @return String title
     */ 
    public String getTitle() {
        return title;
    }

    /**
     * Setter of title
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter of price
     * @return double price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Setter of price
     * @param price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Getter of players
     * @return String players
     */    
    public String getPlayers() {
        return players;
    }

    /**
     * Setter of players
     * @param players
     */    
    public void setPlayers(String players) {
        this.players = players;
    }

    /**
     * Getter of instock
     * @return int instock
     */    
    public int getIn_stock() {
        return instock;
    }

    /**
     * Setter of instock
     * @param in_stock
     */    
    public void setIn_stock(int in_stock) {
        this.instock = in_stock;
    }

    /**
     * Getter of miniage
     * @return String miniage
     */
    public String getMini_age() {
        return miniage;
    }

    /**
     * Setter of miniage
     * @param mini_age
     */    
    public void setMini_age(String mini_age) {
        this.miniage = mini_age;
    }
    
    /**
     * Returns the content of a game in a String
     * @return 
     */        
    @Override
    public String toString() {
        String information = "Game{id=%s, title=%s, price=%s, players=%s, in_stock=%s, mini_age=%s}";
        return String.format(information, this.id, this.title, this.price, this.players, this.instock, this.miniage);
    }

    /**
     * Returns a Game from a ResultSet
     * @param resultSet
     * @return Game
     * @throws java.sql.SQLException 
     */         
    public static Game createFromResultSet(ResultSet resultSet) throws SQLException{
        return new Game(resultSet.getInt("id"), 
                resultSet.getString("title"), 
                resultSet.getDouble("price"), 
                resultSet.getString("players"), 
                resultSet.getInt("instock"),
                resultSet.getString("miniage")
        );
    }

    /**
     * Returns a List of Games from a Database
     * @return List of Games
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException 
     */      
    public static List<Game> all() throws ClassNotFoundException, SQLException{
        List<Game> games;
        try (Connection connection = Database.getConnection(); Statement statement = Database.createStatement(connection)) {
            String query = "SELECT * FROM Games";
            try (ResultSet resultSet = Database.executeQuery(statement, query)) {
                games = new ArrayList<>();
                while(resultSet.next()){
                    games.add(Game.createFromResultSet(resultSet));
                }
            }
        }
        return games;
    }
    
    /**
     * Adds a new Game in the database
     * @param pGame
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException 
     */        
    public static void addGame(Game pGame) throws ClassNotFoundException, SQLException {
        try (Connection connection = Database.getConnection()) {
            String query = "INSERT INTO Games (title, price, players, instock, miniage) VALUES ((?), (?), (?), (?), (?));";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {

                preparedStatement.setString(1, pGame.getTitle());
                preparedStatement.setDouble(2, pGame.getPrice());
                preparedStatement.setString(3, pGame.getPlayers());
                preparedStatement.setInt(4, pGame.getIn_stock());
                preparedStatement.setString(5, pGame.getMini_age());
                preparedStatement.executeUpdate();
            }
        }
    }
    
    /**
     * Removes the Game whose Id in in parameter in the database
     * @param pId
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException 
     */        
    public static void removeGame(int pId) throws ClassNotFoundException, SQLException {
        try (Connection connection = Database.getConnection()) {
            String query = "DELETE FROM Games WHERE id = (?);";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {
                preparedStatement.setInt(1, pId);
                preparedStatement.executeUpdate();
            }
        }
    }
    
    /**
     * Updates the Game whose Id in in parameter in the database
     * @param pId
     * @param pNewGame
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException 
     */       
    public static void updateGame(int pId, Game pNewGame) throws SQLException, ClassNotFoundException {
        try (Connection connection = Database.getConnection()) {
            String query = "UPDATE Games SET title = (?), price = (?), players = (?), instock = (?), miniage = (?)  WHERE id = (?);";
            
            try (PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query)) {
                preparedStatement.setString(1, pNewGame.getTitle());
                preparedStatement.setDouble(2, pNewGame.getPrice());
                preparedStatement.setString(3, pNewGame.getPlayers());
                preparedStatement.setInt(4, pNewGame.getIn_stock());
                preparedStatement.setString(5, pNewGame.getMini_age());
                preparedStatement.setInt(6, pId);
                preparedStatement.executeUpdate();
            }
        }
    }

}
