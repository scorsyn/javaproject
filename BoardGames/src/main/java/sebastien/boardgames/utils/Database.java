    package sebastien.boardgames.utils;

    import java.sql.Connection;
    import java.sql.DriverManager;
    import java.sql.PreparedStatement;
    import java.sql.ResultSet;
    import java.sql.SQLException;
    import java.sql.Statement;

    /**
     *
 * @author sebastien corsyn AS info 2018-2019
     */
    public class Database {

        protected static String dbPath;

   /**
     * Sets the path of the database
     * @param path
     */     
        public static void setDbPath(String path) {
            Database.dbPath = String.format("jdbc:sqlite:%s", path);
        }

   /**
      * Gets connection to a database
     * @return Connection
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
      */           
        public static Connection getConnection() throws ClassNotFoundException, SQLException {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection(Database.dbPath);
            return connection;
        }

   /**
      * Creates a statement
     * @param connection
     * @return Statement
     * @throws java.sql.SQLException
      */           
        public static Statement createStatement(Connection connection) throws SQLException {
            return connection.createStatement();
        }

   /**
      * Prepares a statement from a query
     * @param connection
     * @param query
     * @return PreparedStatement
     * @throws java.sql.SQLException
      */            
        public static PreparedStatement createPreparedStatement(Connection connection, String query) throws SQLException {
            return connection.prepareStatement(query);
        }

   /**
      * Executes a Query
     * @param statement
     * @param query
     * @return ResultSet
     * @throws java.sql.SQLException
      */            
        public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
            return statement.executeQuery(query);
        }
        
   /**
      * Executes a PreaperedQuery
     * @param preparedStatement
     * @return ResultSet
     * @throws java.sql.SQLException
      */            
        public static ResultSet executePreparedQuery(PreparedStatement preparedStatement) throws SQLException{
            return preparedStatement.executeQuery();
        }
    }
