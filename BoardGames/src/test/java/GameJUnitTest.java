/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import sebastien.boardgames.models.Game;

/**
 *
 * @author sebastien
 */
public class GameJUnitTest {
    
    public GameJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // Test getId()
    @Test
    public void testGetId()
    {
        Game _game = new Game(42, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals(42, _game.getId()); 
    } 
    
    // Test getTitle()
    @Test
    public void testGetTitle()
    {
        Game _game = new Game(1, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals("Game title", _game.getTitle()); 
    }
    
    // Test getPrice()
    @Test
    public void testGetPrice()
    {
        Game _game = new Game(1, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals(89.99, _game.getPrice()); 
    }
    
    // Test getPlayers()
    @Test
    public void testGetPlayers()
    {
        Game _game = new Game(1, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals("1-4 players", _game.getPlayers()); 
    }
    
    // Test getInStock()
    @Test
    public void testGetIn_stock()
    {
        Game _game = new Game(1, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals(42, _game.getIn_stock()); 
    }
    
    // Test getInStock()
    @Test
    public void testGetMini_age()
    {
        Game _game = new Game(1, "Game title", 89.99, "1-4 players",42, "10 yo");
        assertEquals("10 yo", _game.getMini_age()); 
    }    
    // Test setTitle()
    @Test
    public void testSetTitle()
    {
        Game _game = new Game();
        _game.setTitle("Game title");
        assertEquals("Game title", _game.getTitle()); 
    }
    
    // Test setPrice()
    @Test
    public void testSetPrice()
    {
        Game _game = new Game();
        _game.setPrice(89.99);
        assertEquals(89.99, _game.getPrice());
    }
    
    // Test setPlayers()
    @Test
    public void testSetPlayers()
    {
        Game _game = new Game();
        _game.setPlayers("1-2 players");
        assertEquals("1-2 players", _game.getPlayers());
    }
    
    // Test setIn_stock()
    @Test
    public void testSetIn_stock()
    {
        Game _game = new Game();
        _game.setIn_stock(42);
        assertEquals(42, _game.getIn_stock());
    }
        
    // Test setMini_age()
    @Test
    public void testSetMini_age()
    {
        Game _game = new Game();
        _game.setMini_age("8 yo");
        assertEquals("8 yo", _game.getMini_age());
    }

}
