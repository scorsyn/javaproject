# JAVA Project

## Board games
A basic java application for learning Maven, Swing, JFrame, JPanel, JDBC, ...

## UML diagramm
![UML diagramm](./UML.png)

## How to use ?
When the application starts, the user has to choose a database thanks to a JFileChooser. The user can change to another database while the application is running.

When the database is opened, all the games stored in it are displayed in a JTable.
- The user can delete a game by clicking on the game row and clicking on the Delete button.
- The user can update an existing game by clicking on the game row, updating the fields below the table and clicking on the Save/Update button.
- The user can add a new game by completing the fields and clicking on the Save/Update button.

__Titles have to be unique.__



