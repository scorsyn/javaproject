CREATE TABLE IF NOT EXISTS "Games" (
`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
`title` TEXT NOT NULL UNIQUE,
`price` DOUBLE NOT NULL,
`players` TEXT NOT NULL,
`instock` INT NOT NULL,
`miniage` TEXT NOT NULL
);

INSERT INTO Games (
title,
price,
players,
instock,
miniage)
VALUES
(
'Monopoly',
19.99,
'2 - 4 players',
3,
'8 yo'
);
